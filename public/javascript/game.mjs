import { getUsername, redirectToLogin, handleСonnectionError } from "./helpers/connectionHelper.mjs";
import {
  createRoom,
  updateRooms,
  joinedToRoom,
  leaveRoom,
  updateUserList,
  changeReadyStatus,
  updateUserStatus
} from "./services/roomService.mjs";
import { prepareToGame, endGame } from "./services/gameService.mjs";
import { handleError } from "./helpers/utils.mjs";
import { updateComment } from "./services/gameService.mjs";

const username = getUsername();
if (!username) {
  redirectToLogin();
}
const socket = io("", { query: { username } });

const roomsPage = document.querySelector("#rooms-page");
const gamePage = document.querySelector("#game-page");
const roomsList = document.querySelector(".rooms");
const createRoomButton = document.querySelector(".button--rooms");
const leaveRoomButton = document.querySelector(".button--back");
const readyRoomButton = document.querySelector(".button--ready");

createRoomButton.addEventListener("click", () => createRoom(socket));
leaveRoomButton.addEventListener("click", () => leaveRoom(socket, gamePage, roomsPage));
readyRoomButton.addEventListener("click", () => changeReadyStatus(socket));

socket.on("CONNECTION_ERROR", handleСonnectionError);
socket.on("CREATE_ROOM_ERROR", handleError);
socket.on("JOIN_ROOM_ERROR", handleError);

socket.on("UPDATE_ROOMS", rooms => updateRooms(roomsList, rooms, socket));

socket.on("JOINED_TO_ROOM", room => joinedToRoom(roomsPage, gamePage, room));

socket.on("UPDATE_USERS", users => updateUserList(users));
socket.on("UPDATE_USER_STATUS", changedStatus => updateUserStatus(changedStatus));

socket.on("GAME_READY", ({ timer, roomName, textId, gameTime }) =>
  prepareToGame(timer, roomName, textId, gameTime, socket)
);

socket.on("NEW_COMMENT", comment => updateComment(comment));

socket.on("GAME_END", updatedRoom => endGame(updatedRoom));
