import { updateGameStartTimer, updateGameTimer, updateEnteredText } from "../helpers/viewHelper.mjs";
import { createElement, addClass } from "../helpers/domHelper.mjs";

export const gameStartTimer = (timeToGameStart, startGame) => {
  let currentTime = timeToGameStart;
  const updateTime = () => {
    updateGameStartTimer(currentTime);
    currentTime--;
    if (currentTime < 0) {
      clearInterval(timerId);
      startGame();
    }
  };
  updateTime();
  const timerId = setInterval(updateTime, 1000);
};

export const getText = async textId => {
  const response = await fetch(`/game/texts/${textId}`);
  return await response.text();
};

export const updateGameText = (text, currentIndex, userChar) => {
  const currentTextChar = text.charAt(currentIndex);
  if (currentTextChar === userChar) {
    const enteredText = text.slice(0, currentIndex);
    const correctTextContainer = createElement({ tagName: "span" });
    const updatedText = enteredText + userChar;
    correctTextContainer.innerText = updatedText;
    const notEnteredText = text.slice(currentIndex + 1);

    if (notEnteredText.length) {
      const notEnteredTextContainer = createElement({ tagName: "span" });
      notEnteredTextContainer.innerText = notEnteredText;
      addClass(correctTextContainer, "text__correct");
      updateEnteredText([correctTextContainer, notEnteredTextContainer]);
    } else {
      addClass(correctTextContainer, "text__correct--full");
      updateEnteredText([correctTextContainer]);
    }
    return true;
  }
  return false;
};

export const updateUserProgress = (text, currentIndex) => {
  return Math.round(100 / (text.length / currentIndex));
};
