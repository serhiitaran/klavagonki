import express from "express";
import http from "http";
import socketIO from "socket.io";
import socketHandler from "./socket";
import routes from "./routes";
import { STATIC_PATH, PORT } from "./config";

const app = express();
const httpServer = http.Server(app);
const io = socketIO(httpServer);

app.use(express.static(STATIC_PATH));
routes(app);

app.get("*", (req, res) => {
  res.redirect("/login");
});

socketHandler(io);

const serverPort = process.env.PORT || PORT;
const serverHost = "0.0.0.0";

httpServer.listen(serverPort, serverHost, () => {
  console.log(`Listen server on port ${serverPort}`);
});
