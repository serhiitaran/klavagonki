import { getTextId } from "./gameHelper";

export const createCommentIntroduce = () => "Мене звати Сергій і сьогодні я буду коментувати цей 100 км заїзд";

export const createCommentPresentation = players => {
  const playersList = players
    .map((player, index) => `<p>${index + 1}. ${player.username} - ${player.car}</p>`)
    .join("\n");
  return `Зустрічайте учасників цього заїзду ${playersList}`;
};

export const createCommentCurrentResults = players => {
  const currentPlayers = [...players];
  currentPlayers.sort((a, b) => b.progress - a.progress);
  const currentPlayersList = currentPlayers
    .map((player, index) => `<p>${index + 1}. ${player.username} - подолав ${player.progress} км </p>`)
    .join("\n");
  return `А зараз саме час повідомити про проміжні результати: ${currentPlayersList}`;
};

export const createCommentFinish = username => {
  return `Гравець ${username} фінішував`;
};

export const createCommentResults = players => {
  return `Результати: ${players} Через 10 секунд ми все приберемо і підготуємо все для нового заїзду.`;
};

export const createCommentAlmostFinish = players => {
  const currentPlayers = [...players];
  currentPlayers.sort((a, b) => b.progress - a.progress);
  const currentPlayersList = currentPlayers
    .map((player, index) => `<p>${index + 1}. ${player.username} - подолав ${player.progress} км </p>`)
    .join("\n");
  return `Майже фініш і маємо такі результати: ${currentPlayersList}`;
};

export const createCommentRandom = () => {
  const randomComments = [
    "Цікавій факт № 1",
    "Цікавий факт № 2",
    "Цікавий факт № 3",
    "Цікавий факт № 4",
    "Цікавий факт № 5",
    "Цікавий факт № 6"
  ];
  const randomCommentId = getTextId(randomComments.length);
  return randomComments[randomCommentId];
};
