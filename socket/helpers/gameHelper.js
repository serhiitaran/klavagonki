export const getTextId = sumOfTexts => {
  const min = 0;
  const max = sumOfTexts - 1;
  let randomTextIndex = min + Math.random() * (max + 1 - min);
  return Math.floor(randomTextIndex);
};

export const sendGameConfig = ({ timer, name, textId, time, io }) => {
  io.in(name).emit("GAME_READY", { timer, roomName: name, textId, gameTime: time });
};

export const updatePlayerProgress = ({ players, username, progress }) => {
  const updatedPlayers = players.map(user => {
    if (user.username === username) {
      const updatedUser = { ...user, progress };
      return updatedUser;
    }
    return user;
  });
  return updatedPlayers;
};

export const sendPlayersProgress = ({ name, players, io }) => {
  io.in(name).emit("UPDATE_USERS", players);
};

export const checkIsGameCanEnd = players => {
  const finishedPlayers = players.filter(player => player.progress === 100);
  return finishedPlayers.length == players.length;
};

export const removePlayerFromGame = ({ players, username }) => {
  return players.filter(player => player.username !== username);
};

export const createWinnerList = ({ players, finishedPlayers }) => {
  const notFinishedPlayers = players.filter(player => player.progress !== 100);
  notFinishedPlayers.sort((a, b) => b.progress - a.progress);
  const finishedPlayersList = finishedPlayers.map(player => `${player.username} - ${player.finishTime} сек`);
  const notFinishedPlayersList = notFinishedPlayers.map(player => `${player.username} - ${player.progress} км`);
  const winnerList = [...finishedPlayersList, ...notFinishedPlayersList].map((player, index) => {
    return `<p>${index + 1}. ${player}</p>`;
  });
  return winnerList.slice(0, 4).join("");
};

export const endCurrentGame = ({ roomName, updatedRoom, io }) => {
  io.in(roomName).emit("GAME_END", updatedRoom);
};

export const addCars = players => {
  const cars = ["Audi", "Ferrari", "BMW", "MercedesM", "porsche"];
  return players.map((player, index) => {
    return { ...player, car: cars[index] };
  });
};
