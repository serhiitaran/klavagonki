export const changeStatus = (room, username, io) => {
  let changedStatus;
  const users = room.users.map(user => {
    if (user.username === username) {
      changedStatus = !user.isReady;
      const updatedUser = { ...user, isReady: changedStatus };
      return updatedUser;
    }
    return user;
  });
  io.in(room.roomName).emit("UPDATE_USERS", users);
  const updatedRoom = { ...room, users };
  return { updatedRoom, changedStatus };
};

export const checkIsGameCanStart = (room, minUsersForGameStart) => {
  const readyUsers = room.users.filter(user => user.isReady);
  return readyUsers.length >= minUsersForGameStart && readyUsers.length === room.users.length;
};

export const updateUserStatus = (changedStatus, updatedRoom, socket) => {
  socket.emit("UPDATE_USER_STATUS", changedStatus);
  return { isRoomUpdated: true, room: updatedRoom };
};

export const getGame = (games, name) => {
  return games.filter(game => game.name == name && !game.isGameEnd)[0];
};
