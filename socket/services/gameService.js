import {
  getTextId,
  sendGameConfig,
  updatePlayerProgress,
  sendPlayersProgress,
  removePlayerFromGame,
  checkIsGameCanEnd,
  createWinnerList,
  endCurrentGame,
  addCars
} from "../helpers/gameHelper";
import { toDefaultRoom } from "../helpers/roomHelper";
import { SECONDS_TIMER_BEFORE_START_GAME, SECONDS_FOR_GAME } from "../config";
import { texts } from "../../data";
import { updateRooms } from "./roomsService";
import { CommentService } from "./commentService";
export class Game {
  constructor(name, players, io, socket) {
    this.name = name;
    this.timer = SECONDS_TIMER_BEFORE_START_GAME;
    this.time = SECONDS_FOR_GAME;
    this.isGameEnd = false;
    this.players = addCars(players);
    this.finishedPlayers = [];
    this.winners = [];
    this.io = io;
    this.socket = socket;
    this.commentService = new CommentService(this.name, this.players, this.io, this.socket);
    this.timeToEnd;
    this.charLeft;
    this.almostFinish = false;
  }

  prepare() {
    const textId = getTextId(texts.length);
    sendGameConfig({ timer: this.timer, name: this.name, textId, time: this.time, io: this.io });
    this.startTimer();
    this.commentService.introduce();
  }

  startTimer() {
    const updateTimeToStart = () => {
      this.timer--;
      if (this.timer < 0) {
        clearInterval(timeToStart);
        this.start();
      }
    };
    updateTimeToStart();
    const timeToStart = setInterval(updateTimeToStart, 1000);
  }

  start() {
    const updateTimeToEnd = () => {
      this.time--;
      if (this.time < 0) {
        this.endGame();
      }
    };
    updateTimeToEnd();
    this.commentService.startGame();
    this.timeToEnd = setInterval(updateTimeToEnd, 1000);
  }

  updateProgress({ username, progress, charsLeft }) {
    this.players = updatePlayerProgress({ name: this.name, players: this.players, username, progress });
    this.commentService.updatePlayers(this.players);
    sendPlayersProgress({ name: this.name, players: this.players, io: this.io });
    if (charsLeft === 30 && !this.almostFinish) {
      this.almostFinish = true;
      this.commentService.almostFinish();
    }
    const isPlayerFinished = progress === 100;
    if (isPlayerFinished) {
      this.commentService.playerFinish(username);
      const finishTime = SECONDS_FOR_GAME - this.time;
      this.finishedPlayers.push({ username, finishTime });
      this.checkGameStatus();
    }
  }

  disconnectUser(username) {
    if (username) {
      this.players = removePlayerFromGame({ players: this.players, username });
      this.checkGameStatus();
    }
  }

  checkGameStatus() {
    this.isGameEnd = checkIsGameCanEnd(this.players);
    if (this.isGameEnd) {
      this.endGame();
    }
  }

  endGame() {
    clearInterval(this.timeToEnd);
    this.winners = createWinnerList({
      players: this.players,
      finishedPlayers: this.finishedPlayers
    });
    this.commentService.results(this.winners);
    const updatedRoom = toDefaultRoom({ roomName: this.name, users: this.players });
    endCurrentGame({ roomName: this.name, updatedRoom, io: this.io });
    setTimeout(() => {
      updateRooms({ isRoomUpdated: true, room: updatedRoom }, this.io);
    }, 10000);
  }
}
