import { getRoom } from "./roomsService";
import { Game } from "./gameService";
import { changeStatus, checkIsGameCanStart, updateUserStatus, getGame } from "../helpers/gamesHelper";
import { MINIMUM_USERS_FOR_GAME_START } from "../config";

let games = [];

export const changeReadyStatus = (roomName, username, socket, io) => {
  const room = getRoom(roomName);
  const { updatedRoom, changedStatus } = changeStatus(room, username, io);
  const isGameCanStart = checkIsGameCanStart(updatedRoom, MINIMUM_USERS_FOR_GAME_START);
  if (isGameCanStart) {
    return createNewGame(updatedRoom, io, socket);
  } else {
    return updateUserStatus(changedStatus, updatedRoom, socket);
  }
};

const createNewGame = (room, io, socket) => {
  const gameStartRoom = { ...room, isGameStart: true };
  const { roomName, users } = gameStartRoom;
  const newGame = new Game(roomName, users, io, socket);
  games.push(newGame);
  newGame.prepare();
  return { isRoomUpdated: true, room: gameStartRoom };
};

export const updateGameProgress = (roomName, username, updatedProgress, charsLeft) => {
  const game = getGame(games, roomName);
  game.updateProgress({ username, progress: updatedProgress, charsLeft });
};

export const updateGameUsers = (roomName, username) => {
  const game = getGame(games, roomName);
  game.disconnectUser(username);
};

export const endEmptyGame = roomName => {
  games = games.map(game => {
    if (game.name === roomName) {
      return { ...game, isGameEnd: true };
    }
    return game;
  });
};
