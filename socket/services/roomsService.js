import { checkIsItemExist } from "../helpers/utils";
import { getRoomByName, refreshRooms, checkIsUserWasInRoom } from "../helpers/roomsHelper";
import { removeUserFromRoom } from "../helpers/roomHelper";
import { updateGameUsers, endEmptyGame } from "./gamesService";

let rooms = [];

export const displayRoomsForUser = socket => {
  socket.emit("UPDATE_ROOMS", rooms);
};

export const checkIsRoomExist = roomName => checkIsItemExist(rooms, roomName, "roomName");

export const getRoom = roomName => getRoomByName(rooms, roomName);

export const addRoom = (createdRoom, io) => {
  const { isRoomCreated, newRoom } = createdRoom;
  if (isRoomCreated) {
    rooms.push(newRoom);
    io.emit("UPDATE_ROOMS", rooms);
  }
};

export const updateRooms = (updatedRoom, io) => {
  const { isRoomUpdated, room } = updatedRoom;
  if (!isRoomUpdated) {
    return;
  }
  const { refreshedRooms } = refreshRooms(rooms, room);
  rooms = [...refreshedRooms];
  io.emit("UPDATE_ROOMS", rooms);
};

export const updateLeavingRooms = (username, socket, io) => {
  const { isUserWasInRoom, room } = checkIsUserWasInRoom(rooms, username);
  if (!isUserWasInRoom) {
    return;
  }
  const updatedRoom = removeUserFromRoom(room, username);
  const { refreshedRooms, status } = refreshRooms(rooms, updatedRoom);
  rooms = [...refreshedRooms];
  if (status === "updated") {
    socket.to(room.roomName).emit("UPDATE_USERS", updatedRoom.users);
    updateGameUsers(room.roomName, username);
  }
  endEmptyGame(updatedRoom.roomName);
  io.emit("UPDATE_ROOMS", rooms);
};
